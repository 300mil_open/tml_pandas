"""TML_PANDAS
Transversal module that gets information over Pandas dataframe's columns
    Diversity
    Dstribution

Notes:
    CODE TAKEN FROM https://github.com/biocore/scikit-bio
    http://scikit-bio.org/docs/0.1.3/index.html
    THERE IS A VERSION PROBLEM BETWEEN NUMPY AND PYTHON 3.8 UNTIL THIS DATE SO INSTALLING THE SCIKIT-BIO IS NOT CURRENTLY WORKING, THEREFORE I USED THEIR SOURCE CODE IN THIS SNIPET

"""


import collections
import numpy as np
import pandas as pd
import mapclassify


__version__ = '0.0.6'
__author__ = 'Andre Resende'


class Diversity:
    '''Computes a given diversity index for a DataFrame df for the given label species

    Example
    -------
    df_tab = pd.read_csv('./data/zoo_tabular.csv')
    div = Diversity(df_tab, species_col = ['lion', 'giraffe', 'elephant', 'bear'], place_col = 'zoo', species_number_col = None)
    div = div.diversity_index(diversity_index='shannon')


    df_series = pd.read_csv('./data/zoo_serial.csv')
    div = Diversity(df_series, species_col = 'animal', place_col = 'zoo', species_number_col = 'number')
    div = div.diversity_index(diversity_index='strong', base=2)

    '''

    def __init__(self, df, species_col, place_col='index', species_number_col=None):
        r"""Diversity object.

        Parameters
        ----------
        df : DataFrame, pandas dataframe
        species_col : Dataframe's columns containing the species labels, str or list
            Single name of categorical columns or list of numeric columns.
        species_col : Dataframe's columns containing the species labels, str or list
            Single name of categorical columns or list of numeric columns.
        place_col : Dataframe's column name with a label to use as aggregation for the indexes, str
            Single name of categorical columns or default dataframe's index is used.
        species_number_col : Dataframe's column containing the count number of each species, str
            Single name of categorical column or default will realize a count of all the species for a given index.

        Returns
        -------
        Diversity object
            Object to extract all diversity indexes from.

        """
        self.df = self.makeDF(df=df, species_col=species_col, place_col=place_col,
                              species_number_col=species_number_col)

    def makeDF(self, df, species_col, place_col='index', species_number_col=None):
        r"""Builds a tabular DataFrame in the format to be applied the indexes.

        Parameters
        ----------
        df : DataFrame, pandas dataframe
        species_col : Dataframe's columns containing the species labels, str or list
            Single name of categorical columns or list of numeric columns.
        species_col : Dataframe's columns containing the species labels, str or list
            Single name of categorical columns or list of numeric columns.
        place_col : Dataframe's column name with a label to use as aggregation for the indexes, str
            Single name of categorical columns or default dataframe's index is used.
        species_number_col : Dataframe's column containing the count number of each species, str
            Single name of categorical column or default will realize a count of all the species for a given index.

        Returns
        -------
        Pandas DataFrame
            Dataframe in the correct format to the Diversity index functions to be applied

        """
        species_col = [species_col] if type(species_col) is str else species_col
        dfOut = df.copy()

        if place_col != 'index':
            dfOut.set_index(place_col, inplace=True)

        if len(species_col) == 1:
            if species_number_col:
                dfOut = pd.pivot_table(dfOut, index=dfOut.index, columns=species_col, aggfunc=np.sum)
            else:
                dfOut = pd.pivot_table(dfOut, index=dfOut.index, columns=species_col, aggfunc=np.size)

        dfOut = dfOut.groupby(dfOut.index)[[x for x in dfOut.columns]].sum()
        return dfOut

    def _validate_counts_vector(self, counts, suppress_cast=False):
        """Validate and convert input to an acceptable counts vector type.

        Notes
        -----
        may not always return a copy of `counts`!
        """
        counts = np.asarray(counts)
        if not np.all(np.isreal(counts)):
            raise ValueError("Counts vector must contain real-valued entries.")
        if counts.ndim != 1:
            raise ValueError("Only 1-D vectors are supported.")
        elif (counts < 0).any():
            raise ValueError("Counts vector cannot contain negative values.")

        return counts

    def observed_otus(self, counts):
        """Calculate the number of distinct OTUs.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        int
            Distinct OTU count.
        """
        counts = self._validate_counts_vector(counts)
        return (counts != 0).sum()

    def berger_parker(self, counts):
        r"""Calculate Berger-Parker dominance.
        Berger-Parker dominance is defined as the fraction of the sample that
        belongs to the most abundant OTU:
        .. math::
           d = \frac{N_{max}}{N}
        where :math:`N_{max}` is defined as the number of individuals in the most
        abundant OTU (or any of the most abundant OTUs in the case of ties), and
        :math:`N` is defined as the total number of individuals in the sample.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            Berger-Parker dominance.

        Notes
        -----
        Berger-Parker dominance is defined in [1]_. The implementation here is
        based on the description given in the SDR-IV online manual [2]_.

        References
        ----------
        .. [1] Berger & Parker (1970). SDR-IV online help.
        .. [2] http://www.pisces-conservation.com/sdrhelp/index.html
        """
        counts = self._validate_counts_vector(counts)
        return counts.max() / counts.sum()

    def dominance(self, counts):
        r"""Calculate dominance.
        Dominance is defined as
        .. math::
           \sum{p_i^2}
        where :math:`p_i` is the proportion of the entire community that OTU
        :math:`i` represents.
        Dominance can also be defined as 1 - Simpson's index. It ranges between
        0 and 1.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            Dominance.

        See Also
        --------
        simpson

        Notes
        -----
        The implementation here is based on the description given in [1]_.

        References
        ----------
        .. [1] http://folk.uio.no/ohammer/past/diversity.html
        """
        counts = self._validate_counts_vector(counts)
        freqs = counts / counts.sum()
        return (freqs * freqs).sum()

    def enspie(self, counts):
        r"""Calculate ENS_pie alpha diversity measure.
        ENS_pie is equivalent to ``1 / dominance``:
        .. math::
           ENS_{pie} = \frac{1}{\sum_{i=1}^s{p_i^2}}
        where :math:`s` is the number of OTUs and :math:`p_i` is the proportion of
        the community represented by OTU :math:`i`.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            ENS_pie alpha diversity measure.

        See Also
        --------
        dominance

        Notes
        -----
        ENS_pie is defined in [1]_.

        References
        ----------
        .. [1] Chase and Knight (2013). "Scale-dependent effect sizes of ecological
           drivers on biodiversity: why standardised sampling is not enough".
           Ecology Letters, Volume 16, Issue Supplement s1, pgs 17-26.
        """
        counts = self._validate_counts_vector(counts)
        return 1 / self.dominance(counts)

    def kempton_taylor(self, counts, lower_quantile=0.25, upper_quantile=0.75):
        """Calculate Kempton-Taylor Q index of alpha diversity.
        Estimates the slope of the cumulative abundance curve in the interquantile
        range. By default, uses lower and upper quartiles, rounding inwards.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.
        lower_quantile : float, optional
            Lower bound of the interquantile range. Defaults to lower quartile.
        upper_quantile : float, optional
            Upper bound of the interquantile range. Defaults to upper quartile.

        Returns
        -------
        double
            Kempton-Taylor Q index of alpha diversity.

        Notes
        -----
        The index is defined in [1]_. The implementation here is based on the
        description given in the SDR-IV online manual [2]_.
        The implementation provided here differs slightly from the results given in
        Magurran 1998. Specifically, we have 14 in the numerator rather than 15.
        Magurran recommends counting half of the OTUs with the same # counts as the
        point where the UQ falls and the point where the LQ falls, but the
        justification for this is unclear (e.g. if there were a very large # OTUs
        that just overlapped one of the quantiles, the results would be
        considerably off). Leaving the calculation as-is for now, but consider
        changing.

        References
        ----------
        .. [1] Kempton, R. A. and Taylor, L. R. (1976) Models and statistics for
           species diversity. Nature, 262, 818-820.
        .. [2] http://www.pisces-conservation.com/sdrhelp/index.html
        """
        counts = self._validate_counts_vector(counts)
        n = len(counts)
        lower = int(np.ceil(n * lower_quantile))
        upper = int(n * upper_quantile)
        sorted_counts = np.sort(counts)
        return (upper - lower) / np.log(sorted_counts[upper] / sorted_counts[lower])

    def margalef(self, counts):
        r"""Calculate Margalef's richness index.
        Margalef's D is defined as:
        .. math::
           D = \frac{(S - 1)}{\ln N}
        where :math:`S` is the number of OTUs and :math:`N` is the total number of
        individuals in the sample.
        Assumes log accumulation.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            Margalef's richness index.

        Notes
        -----
        Based on the description in [1]_.

        References
        ----------
        .. [1] Magurran, A E 2004. Measuring biological diversity. Blackwell. pp.
           76-77.
        """
        counts = self._validate_counts_vector(counts)
        return (self.observed_otus(counts) - 1) / np.log(counts.sum())

    def mcintosh(self, counts):
        r"""Calculate McIntosh dominance index D.
        McIntosh dominance index D is defined as:
        .. math::
           D = \frac{N - U}{N - \sqrt{N}}
        where :math:`N` is the total number of individuals in the sample and
        :math:`U` is defined as:
        .. math::
           U = \sqrt{\sum{{n_i}^2}}
        where :math:`n_i` is the number of individuals in the :math:`i^{\text{th}}`
        OTU.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            McIntosh dominance index D.

        See Also
        --------
        mcintosh_e

        Notes
        -----
        The index was proposed in [1]_. The implementation here is based on the
        description given in the SDR-IV online manual [2]_.

        References
        ----------
        .. [1] McIntosh, R. P. 1967 An index of diversity and the relation of
           certain concepts to diversity. Ecology 48, 1115-1126.
        .. [2] http://www.pisces-conservation.com/sdrhelp/index.html
        """
        counts = self._validate_counts_vector(counts)
        u = np.sqrt((counts * counts).sum())
        n = counts.sum()
        return (n - u) / (n - np.sqrt(n))

    def menhinick(self, counts):
        r"""Calculate Menhinick's richness index.
        Menhinick's richness index is defined as:
        .. math::
           D_{Mn} = \frac{S}{\sqrt{N}}
        where :math:`S` is the number of OTUs and :math:`N` is the total number of
        individuals in the sample.
        Assumes square-root accumulation.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            Menhinick's richness index.

        Notes
        -----
        Based on the description in [1]_.

        References
        ----------
        .. [1] Magurran, A E 2004. Measuring biological diversity. Blackwell. pp.
           76-77.
        """
        counts = self._validate_counts_vector(counts)
        return self.observed_otus(counts) / np.sqrt(counts.sum())

    def shannon(self, counts, base=2):
        r"""Calculate Shannon entropy of counts, default in bits.
        Shannon-Wiener diversity index is defined as:
        .. math::
           H = -\sum_{i=1}^s\left(p_i\log_2 p_i\right)
        where :math:`s` is the number of OTUs and :math:`p_i` is the proportion of
        the community represented by OTU :math:`i`.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.
        base : scalar, optional
            Logarithm base to use in the calculations.

        Returns
        -------
        double
            Shannon diversity index H.

        Notes
        -----
        The implementation here is based on the description given in the SDR-IV
        online manual [1]_ except that the default logarithm base used here is 2
        instead of :math:`e`.

        References
        ----------
        .. [1] http://www.pisces-conservation.com/sdrhelp/index.html
        """
        counts = self._validate_counts_vector(counts)
        freqs = counts / counts.sum()
        nonzero_freqs = freqs[freqs.nonzero()]
        return -(nonzero_freqs * np.log(nonzero_freqs)).sum() / np.log(base)

    def simpson(self, counts):
        r"""Calculate Simpson's index.
        Simpson's index is defined as ``1 - dominance``:
        .. math::
           1 - \sum{p_i^2}
        where :math:`p_i` is the proportion of the community represented by OTU
        :math:`i`.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            Simpson's index.

        See Also
        --------
        dominance

        Notes
        -----
        The implementation here is ``1 - dominance`` as described in [1]_. Other
        references (such as [2]_) define Simpson's index as ``1 / dominance``.

        References
        ----------
        .. [1] http://folk.uio.no/ohammer/past/diversity.html
        .. [2] http://www.pisces-conservation.com/sdrhelp/index.html
        """
        counts = self._validate_counts_vector(counts)
        return 1 - self.dominance(counts)

    def strong(self, counts):
        r"""Calculate Strong's dominance index.
        Strong's dominance index is defined as:
        .. math::
           D_w = max_i[(\frac{b_i}{N})-\frac{i}{S}]
        where :math:`b_i` is the sequential cumulative totaling of the
        :math:`i^{\text{th}}` OTU abundance values ranked from largest to smallest,
        :math:`N` is the total number of individuals in the sample, and
        :math:`S` is the number of OTUs in the sample. The expression in brackets
        is computed for all OTUs, and :math:`max_i` denotes the maximum value in
        brackets for any OTU.

        Parameters
        ----------
        counts : 1-D array_like, int
            Vector of counts.

        Returns
        -------
        double
            Strong's dominance index (Dw).

        Notes
        -----
        Strong's dominance index is defined in [1]_. The implementation here is
        based on the description given in the SDR-IV online manual [2]_.

        References
        ----------
        .. [1] Strong, W. L., 2002 Assessing species abundance uneveness within and
           between plant communities. Community Ecology, 3, 237-246.
        .. [2] http://www.pisces-conservation.com/sdrhelp/index.html
        """
        counts = self._validate_counts_vector(counts)
        n = counts.sum()
        s = self.observed_otus(counts)
        i = np.arange(1, len(counts) + 1)
        sorted_sum = np.sort(counts)[::-1].cumsum()
        return (sorted_sum / n - (i / s)).max()

    def diversity_index(self, diversity_index, base=2, lower_quantile=0.25, upper_quantile=0.75):
        r"""Calculate the given diversity index.

        Parameters
        ----------
        diversity_index : berger_parker, dominance, enspie, kempton_taylor, margalef, mcintosh, menhinick, simpson, strong, shannon, str
        base : scalar applies only  for Shannon Index, optional
            Logarithm base to use in the calculations.
        lower_quantile : float applies only for Kempton Taylor Index, optional
            Lower bound of the interquantile range. Defaults to lower quartile.
        upper_quantile : float applies only for Kempton Taylor Index, optional
            Upper bound of the interquantile range. Defaults to upper quartile.

        Returns
        -------
        Pandas DataFrame
            With the given dataframe index and the diversity index calculated.

        Notes
        -----
        The cross package was built over Scikit-Bio package[1]

        ----------
        .. [1] https://github.com/biocore/scikit-bio
        """

        dfOut = self.df.copy()

        if diversity_index == 'shannon':
            dfOut['shannon'] = dfOut.apply(lambda row: self.shannon([row[col] for col in dfOut.columns], base=base),
                                           axis=1)
            dfOut = dfOut[['shannon']]
        elif diversity_index == 'strong':
            dfOut['strong'] = dfOut.apply(lambda row: self.strong([row[col] for col in dfOut.columns]), axis=1)
            dfOut = dfOut[['strong']]
        elif diversity_index == 'simpson':
            dfOut['simpson'] = dfOut.apply(lambda row: self.simpson([row[col] for col in dfOut.columns]), axis=1)
            dfOut = dfOut[['simpson']]
        elif diversity_index == 'menhinick':
            dfOut['menhinick'] = dfOut.apply(lambda row: self.menhinick([row[col] for col in dfOut.columns]), axis=1)
            dfOut = dfOut[['menhinick']]
        elif diversity_index == 'mcintosh':
            dfOut['mcintosh'] = dfOut.apply(lambda row: self.mcintosh([row[col] for col in dfOut.columns]), axis=1)
            dfOut = dfOut[['mcintosh']]
        elif diversity_index == 'margalef':
            dfOut['margalef'] = dfOut.apply(lambda row: self.margalef([row[col] for col in dfOut.columns]), axis=1)
            dfOut = dfOut[['margalef']]
        elif diversity_index == 'kempton_taylor':
            dfOut['kempton_taylor'] = dfOut.apply(
                lambda row: self.kempton_taylor([row[col] for col in dfOut.columns], lower_quantile=lower_quantile,
                                                upper_quantile=upper_quantile), axis=1)
            dfOut = dfOut[['kempton_taylor']]
        elif diversity_index == 'enspie':
            dfOut['enspie'] = dfOut.apply(lambda row: self.enspie([row[col] for col in dfOut.columns]), axis=1)
            dfOut = dfOut[['enspie']]
        elif diversity_index == 'dominance':
            dfOut['dominance'] = dfOut.apply(lambda row: self.dominance([row[col] for col in dfOut.columns]), axis=1)
            dfOut = dfOut[['dominance']]
        elif diversity_index == 'berger_parker':
            dfOut['berger_parker'] = dfOut.apply(lambda row: self.berger_parker([row[col] for col in dfOut.columns]),
                                                 axis=1)
            dfOut = dfOut[['berger_parker']]
        else:
            print(
                'Please enter one of the following diversity index: berger_parker, dominance, enspie, kempton_taylor, margalef, mcintosh, menhinick, simpson, strong, shannon.')
            return

        dfOut.reset_index(inplace=True)
        return dfOut


class Distribution:
    '''Computes a given data distribution for a DataFrame df for the given label species

    Example
    -------
    df_series = pd.read_csv('./data/zoo_serial.csv')
    dist = Distribution(df)
    dist = div.equal_interval('number')

    '''
    def __init__(self, df):
        r"""Distribution object.

        Parameters
        ----------
        df : DataFrame, pandas dataframe

        Returns
        -------
        Distribution object
            Object to extract data distribution from.

        """
        self.df = df

    def equal_interval(self, col):
        r"""Calculate equal interval distribution for the given columns.

        Parameters
        ----------
        col : Dataframe column name, str
            Numeric column.

        Returns
        -------
        Panda's Dataframe
            With the binning limits and number of elements in each bin.

        Notes
        -----
        Alson known as uniform distribuion [1]_.

        References
        ----------
        .. [1] https://en.wikipedia.org/wiki/Continuous_uniform_distribution
        """

        y = self.df[col].dropna()
        y = mapclassify.EqualInterval(y)
        y = pd.DataFrame(data={'bins': y.bins, 'counts': y.counts})
        return y

    def jenks(self, col, n):
        r"""Calculate a Caspall Jenks distribution for the given columns.

        Parameters
        ----------
        col : Dataframe column name, str
            Numeric column.
        n : Numeric, int
            Number of bins to distribute the column with.

        Returns
        -------
        Panda's Dataframe
            With the binning limits and number of elements in each bin.

        Notes
        -----
        Jenks optimization method, also called the Jenks natural breaks classification method [1]_.

        References
        ----------
        .. [1] https://en.wikipedia.org/wiki/Jenks_natural_breaks_optimization
        """
        y = self.df[col].dropna()
        y = mapclassify.JenksCaspall(y, k=n)
        y = pd.DataFrame(data={'bins': y.bins, 'counts': y.counts})
        return y

    def natural_breaks(self, col, n):
        r"""Calculate a Natural Breaks distribution for the given columns.

        Parameters
        ----------
        col : Dataframe column name, str
            Numeric column.
        n : Numeric, int
            Number of bins to distribute the column with.

        Returns
        -------
        Panda's Dataframe
            With the binning limits and number of elements in each bin.

        Notes
        -----
        See Jenks natural breaks optimization [1]_.

        References
        ----------
        .. [1] https://en.wikipedia.org/wiki/Jenks_natural_breaks_optimization
        """
        y = self.df[col].dropna()
        y = mapclassify.NaturalBreaks(y, k=n)
        y = pd.DataFrame(data={'bins': y.bins, 'counts': y.counts})
        return y

    def quantiles(self, col, n):
        r"""Calculate a quantile distribution for the given columns.

        Parameters
        ----------
        col : Dataframe column name, str
            Numeric column.
        n : Numeric, int
            Number of bins to distribute the column with.

        Returns
        -------
        Panda's Dataframe
            With the binning limits and number of elements in each bin.

        Notes
        -----
        Method that distributes a set of values into groups that contain an equal number of values. [1]_.

        References
        ----------
        .. [1] http://wiki.gis.com/wiki/index.php/Quantile
        """
        y = self.df[col].dropna()
        y = mapclassify.Quantiles(y, k=n)
        y = pd.DataFrame(data={'bins': y.bins, 'counts': y.counts})
        return y

    def percentiles(self, col, perc):
        r"""Calculate a quantile distribution for the given columns.

        Parameters
        ----------
        col : Dataframe column name, str
            Numeric column.
        perc : Numeric list, list
            List of percentiles to use as binning limits.

        Returns
        -------
        Panda's Dataframe
            With the binning limits and number of elements in each bin.

        Notes
        -----
        Look for Percentile Rank. [1]_.

        References
        ----------
        .. [1] https://en.wikipedia.org/wiki/Percentile
        """
        y = self.df[col].dropna()
        y = mapclassify.Percentiles(y, pct=perc)
        y = pd.DataFrame(data={'bins': y.bins, 'counts': y.counts})
        return y

    def std_dev(self, col):
        r"""Calculate a mean standard deviation distribution for the given columns.

        Parameters
        ----------
        col : Dataframe column name, str
            Numeric column.

        Returns
        -------
        Panda's Dataframe
            With the binning limits and number of elements in each bin.

        Notes
        -----
        Look for Standard Deviation. [1]_.

        References
        ----------
        .. [1] https://en.wikipedia.org/wiki/Standard_deviation
        """
        y = self.df[col].dropna()
        y = mapclassify.StdMean(y)
        y = pd.DataFrame(data={'bins': y.bins, 'counts': y.counts})
        return y

    def custom(self, col, bins):
        r"""Calculate a mean standard deviation distribution for the given columns.

        Parameters
        ----------
        col : Dataframe column name, str
            Numeric column.
        bins : Numeric list, list
            List of limits for the customized binning.

        Returns
        -------
        Panda's Dataframe
            With the binning limits and number of elements in each bin.

        """
        y = self.df[col].dropna()
        y = mapclassify.UserDefined(y, bins=bins)
        y = pd.DataFrame(data={'bins': y.bins, 'counts': y.counts})
        return y


####################################################################################################

####################################################################################################

if __name__ == '__main__':
    print()