from setuptools import setup

setup(
    name='tml_pandas',
    version='0.0.6',
    packages=['tml_pandas'],
    url='https://gitlab.com/300mil/tml_pandas',
    license='unlicense',
    author='Andre Resende',
    author_email='andre@300000kms.net',
    description='Exploratory functions based on Pandas DataFrames',
    install_requires=[
        'numpy >= 1.17.4',
        'pandas >= 1.2.0',
        'mapclassify >= 2.4.2'
    ]
)
